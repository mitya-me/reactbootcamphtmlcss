
const loginForm = document.querySelector('.main__login-form');
const loginFormEmail = document.querySelector('.login-form__input-email');
const loginFormPassword = document.querySelector('.login-form__input-pass');
const loginFormError = document.querySelector('.login-form__error-block');
const loginFormSubmitButton = document.querySelector('.login-form__input-submit');

async function login({ email, password }) {
  return new Promise((resolve, reject) => {
    if (email === 'user@example.com' && password === 'mercdev') {
      resolve({
        name: 'React Bootcamp',
        photoUrl: 'https://picsum.photos/200'
      })
    } else {
      reject(new Error('Incorrect email or password'))
    }
  })
}

async function tryLogin() {
  const email = loginFormEmail.value;
  const password = loginFormPassword.value;

  disableLoginForm();
  hideLoginError();

  try {
    const user = await login({ email, password });
    showUserProfile(user);
  } catch (error) {
    showLoginError(error.message);
  } finally {
    enableLoginForm();
  }
}

function showLoginValidationError() {
  loginFormEmail.classList.add('text-input--invalid');
  loginFormPassword.classList.add('text-input--invalid');
}

function hideLoginValidationError() {
  loginFormEmail.classList.remove('text-input--invalid');
  loginFormPassword.classList.remove('text-input--invalid');
}

function showLoginError(message) {
  loginFormError.innerText = message;
  loginFormError.removeAttribute('hidden');
  showLoginValidationError();
}

function hideLoginError() {
  loginFormError.setAttribute('hidden', 'true');
  hideLoginValidationError();
}

function disableLoginForm() {
  loginFormEmail.setAttribute('disabled', 'true');
  loginFormPassword.setAttribute('disabled', 'true');
  loginFormSubmitButton.setAttribute('disabled', 'true');
}

function enableLoginForm() {
  loginFormEmail.removeAttribute('disabled');
  loginFormPassword.removeAttribute('disabled');
  loginFormSubmitButton.removeAttribute('disabled');
}

loginFormSubmitButton.addEventListener('click', tryLogin);
loginFormEmail.addEventListener('input', hideLoginValidationError);
loginFormPassword.addEventListener('input', hideLoginValidationError);

const userProfile = document.querySelector('.main__user-profile');
const userProfileName = document.querySelector('.profile-user__name');
const userProfileAvatar = document.querySelector('.profile-user__avatar-img');
const userProfileLogoutButton = document.querySelector('.profile-user__logout-btn');

function showUserProfile(user) {
  loginForm.setAttribute('hidden', 'true');

  userProfileName.innerText = user.name;
  userProfileAvatar.setAttribute('src', user.photoUrl);
  userProfile.removeAttribute('hidden');
}

function logout() {
  window.location.reload();
}

userProfileLogoutButton.addEventListener('click', logout);
